class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.integer :stage
      t.datetime :enter
      t.datetime :leave
      t.float :total_time
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
