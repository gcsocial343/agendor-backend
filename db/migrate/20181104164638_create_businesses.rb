class CreateBusinesses < ActiveRecord::Migration[5.2]
  def change
    create_table :businesses do |t|
      t.integer :stage, default: 1
      t.string :name
      t.string :company
      t.decimal :value
      t.datetime :closed_at
      t.timestamps
    end
  end
end
