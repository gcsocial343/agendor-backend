class BusinessesController < ApplicationController
    before_action :set_business, only: [ :update ]

    def index
        @businesses = Business.all
        json_response(@businesses)
    end

    def create
        @business = Business.create!(business_params)
        json_response(@business)
    end

    def update
        @params = update_params
        @business.update!(@params)
        head :no_content
    end
    private

    def business_params
        params.permit(:name,:company,:value)
    end

    def update_params
        params.permit(:stage)
    end

    def set_business
        @business = Business.find(params[:id])
    end
end
