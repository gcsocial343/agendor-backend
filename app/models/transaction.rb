class Transaction < ApplicationRecord
  belongs_to :business

  validates_presence_of :enter
end
