class Business < ApplicationRecord
  has_many :transactions, dependent: :destroy

  validates :name, :company, :value, presence: true , on: :create
  validates :value, numericality: true, on: :create
  validates :stage, presence: true ,  on: :update 
  
  validate :should_transact?, :is_valid_stage?, on: :update

  
  after_create do
    add_transaction
  end

  after_update do
    new_transaction
    add_transaction
  end


  def is_valid_stage?
    if !(1..6).include?(stage) then
      errors.add(:stage, 'is invalid')
    end    
  end

  def should_transact?
    if stage != 6 and stage_was == 5 then
      errors.add(:stage, 'is invalid')
    end
  end

  def add_transaction
    Transaction.create(enter: Time.now, business: self, stage: self.stage )
  end

  def new_transaction
    @now = Time.now
    @enter_time = self.transactions.last.enter
    self.transactions.last.update_attribute( :leave ,  @now)
    self.transactions.last.update_attribute( :total_time ,  @enter_time - @now)
  end
end
