Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'businesses', to: 'businesses#index'
  post 'businesses', to: 'businesses#create'
  put 'businesses/:id', to: 'businesses#update'

end
