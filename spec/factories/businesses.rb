FactoryBot.define do
    factory :business do
        name { Faker::StarWars.character }
        company { Faker::Company.name }
        value { Faker::Commerce.price(range = 5000..100000.0, as_string = false) }
        not_closed
        trait :not_closed do
            stage { Faker::Number.between(1,4) }
        end

        trait :closed do
            stage{ 5}
        end
    end
end