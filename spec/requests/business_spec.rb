require 'rails_helper'

RSpec.describe 'Business API', type: :request do
    let!(:businesses) { create_list(:business, 9 )}
    let(:business_id) { businesses.first.id }
    let!(:closed_business ) { create( :business , :closed)}
    describe 'GET /businesses' do
        before { get '/businesses'}

        it 'return_business' do
            expect(json).not_to be_empty
            expect(json.size).to eq 10
        end

        it 'returns status code 200' do
            expect(response).to have_http_status(200)
        end
    end

    describe 'POST /businesses' do
        let(:valid_attributes) {{ name: 'Learn Elm', company: 'Microsoft', value: '20000.25' }}

        context 'when the request is valid' do
            before { post '/businesses' , params: valid_attributes }

            it 'create a business' do
                expect(json['name']).to eq('Learn Elm')
            end

            it 'returns status code 201' do
                expect(response).to have_http_status(200)
            end
        end

        context 'when the request is invalid' do

            context 'name is empty' do
                before { post '/businesses' , params:{ company: 'Microsoft', value: '20000.25' }}
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                  end
            
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Name can't be blank/)
                end
            end

            context 'business is empty' do
                before { post '/businesses' , params: { name: 'Learn Elm',  value: '20000.25' }}
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                  end
            
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Company can't be blank/)
                end
            end

            context 'value is empty' do
                before { post '/businesses' , params: { name: 'Learn Elm', company: 'Microsoft' }}
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                  end
            
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Value can't be blank/)
                end
            end    
            
            context 'value is not a number' do
                before { post '/businesses' , params: { name: 'Learn Elm', company: 'Microsoft', value: 'abacaxi' }}
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                end
            
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Value is not a number/)
                end
            end
        end
    end
    
    describe 'PUT /businesses/:id' do

        context 'when the record exists' do
            context 'when the request is valid' do  
                before { put "/businesses/#{business_id}", params: { stage: 3 } }
          
                it 'updates the record' do
                  expect(response.body).to be_empty
                end
          
                it 'returns status code 204' do
                  expect(response).to have_http_status(204)
                end
            end
            context 'when the request in not valid' do
                context 'invalid stage'
                before { put "/businesses/#{business_id}" , params: { stage: 22 }  }
          
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                end
          
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Stage is invalid/)
                end
            end
            context 'when the request in not valid' do
                context 'invalid stage'
                before { put "/businesses/#{business_id}" , params: { stage: 0 }  }
          
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                end
          
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Stage is invalid/)
                end
            end
            context 'when the request in not valid' do
                context 'invalid stage'
                before { put "/businesses/#{closed_business.id}" , params: { stage: 4 }  }
          
                it 'returns status code 422' do
                    expect(response).to have_http_status(422)
                end
          
                it 'returns a validation failure message' do
                    expect(response.body)
                        .to match(/Validation failed: Stage is invalid/)
                end
            end
        end
        
    end
end