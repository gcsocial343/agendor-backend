require 'rails_helper'

RSpec.describe Transaction, type: :model do
  it { should belong_to(:business)}
  it { should validate_presence_of (:enter)}
end
